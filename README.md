CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

intends to improve the default Drupal, providing a fast and full access.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/srikanthg/3116131




REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Srikanth Ganta - https://www.drupal.org/u/srikanthg-0

This project has been sponsored by:
 * Wipro
